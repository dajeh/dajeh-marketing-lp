import { useMatomo } from '@datapunt/matomo-tracker-react';
import { useEffect, useState } from 'react';
import AppStore from '../components/AppStore';
import ContactModal, { ContactModalId } from '../components/ContactModal';
import CallToActionFooter from '../components/CallToActionFooter';
import Features from '../components/Features';
import Hero from '../components/Hero';
import Layout from '../components/Layout';

export default function Home() {
    const [contactSalesModalIsOpen, setContactSalesModalIsOpen] = useState(false);
    const [requestTestAccountModalIsOpen, setRequestTestAccountModalIsOpen] =
        useState(false);

    const { trackPageView, trackEvent } = useMatomo()

    // Track page view
    useEffect(() => {
        trackPageView({})
    }, [])

    const onClickTryForFreeIntroduction = () => {
        setRequestTestAccountModalIsOpen(true);
        trackEvent({
            category: 'try-for-free',
            action: 'click',
            name: 'home-intro'
        })
    }

    const onEarlyBirdClick = () => {
        setContactSalesModalIsOpen(true);
        trackEvent({
            category: 'contact-sales',
            action: 'click',
            name: 'home-early-bird'
        })
    }

    return (
        <Layout>
            <Hero
                onClickTryForFree={onClickTryForFreeIntroduction}
            />
            <Features />
            <AppStore />
            <CallToActionFooter
                openContactSalesModal={onEarlyBirdClick}
            />

            {contactSalesModalIsOpen && (
                <ContactModal
                    onClose={() => setContactSalesModalIsOpen(false)}
                    type={ContactModalId.contactSales}
                />
            )}

            {requestTestAccountModalIsOpen && (
                <ContactModal
                    onClose={() => setRequestTestAccountModalIsOpen(false)}
                    type={ContactModalId.requestTestAccount}
                />
            )}
        </Layout>
    );
}
