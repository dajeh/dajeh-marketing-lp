import Layout from '../components/Layout';
import { getPageTitle } from '../functions';

export default function Impressum() {
  return (
    <Layout title="Impressum">
      <div className="datenschutz container mx-auto px-4 py-24">
        <h1>Impressum</h1>

        <p>dajeh.de ist ein Angebot der Kalaflax GmbH</p>

        <p>
          Kalaflax GmbH <br />
          Klaus-Kordel-Straße 4 <br />
          54296 Trier <br />
          <a href="tel:+4965193748700">0651 9374 870 0</a>
        </p>

        <p>
          <a href="mailto:hallo@dajeh.de">hallo@dajeh.de</a>
        </p>

        <h3>Geschäftsführer</h3>

        <p>
          Mike Bastian <br />
          Tobias Orth
        </p>

        <h3>Registergericht</h3>

        <p>
          Amtsgericht Wittlich <br />
          HRB 45110
        </p>

        <h3>USt-IdNr</h3>

        <p>DE296064080</p>

        <h2>Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV</h2>

        <p>Mike Bastian, Tobias Orth</p>

        <h2>Copyright</h2>

        <p>
          Das Urheberrecht für veröffentlichte Beiträge liegt ausschließlich bei
          der Kalaflax GmbH. Vervielfältigungen sowie Nachdruck oder sonstige
          Verwertung von Texten nur mit schriftlicher Genehmigung des
          Herausgebers. Fremdbeiträge geben nicht unbedingt die Meinung des
          Herausgebers wieder.
        </p>

        <h2>Rechtliche Hinweise</h2>

        <p>
          Der Inhalt dieser Website ist urheberrechtlich geschützt und steht
          ausschließlich der Kalaflax GmbH&nbsp;zu. Ohne die explizite Erlaubnis
          auf dieser Website oder die schriftliche Zustimmung der
          Kalaflax&nbsp;GmbH ist die Vervielfältigung oder sonstige Verwendung
          der bereitgestellten Inhalte und Informationen, insbesondere von
          Grafiken oder sonstigen Dokumenten, nicht gestattet.
        </p>
      </div>
    </Layout>
  );
}
