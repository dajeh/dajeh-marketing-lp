import { createInstance, MatomoProvider } from '@datapunt/matomo-tracker-react'
import { getCookieConsentValue } from 'react-cookie-consent';
import { ThemeProvider } from 'styled-components'
import { CookieBanner } from '../components/CookieBanner';
import GlobalStyle from '../styles/global';
import '../styles/globals.css';
import Theme from '../styles/theme';

let matomoInstance = getMatomoInstance();

function getMatomoInstance() {
    return createInstance({
        urlBase: 'https://kalaflax.matomo.cloud/',
        siteId: 3,
        // userId: 'UID76903202', // optional, default value: `undefined`.
        // trackerUrl: 'https://LINK.TO.DOMAIN/tracking.php', // optional, default value: `${urlBase}matomo.php`
        // srcUrl: 'https://LINK.TO.DOMAIN/tracking.js', // optional, default value: `${urlBase}matomo.js`
        // disabled: false, // optional, false by default. Makes all tracking calls no-ops if set to true.
        // heartBeat: { // optional, enabled by default
        //   active: true, // optional, default value: true
        //   seconds: 10 // optional, default value: `15
        // },
        // linkTracking: false, // optional, default value: true
        configurations: { // optional, default value: {}
            // any valid matomo configuration, all below are optional
            disableCookies: !getCookieConsentValue(),
        }
    })
}

function updateMatomoInstance() {
    if (matomoInstance) {
        matomoInstance.stopObserving();
    }

    matomoInstance = getMatomoInstance();
}

function MyApp({ Component, pageProps }) {
    return (
        <>
            <GlobalStyle />
            <ThemeProvider theme={Theme}>
                <MatomoProvider value={matomoInstance}>
                    <Component {...pageProps} />
                    <CookieBanner
                        onAccept={updateMatomoInstance}
                        onDecline={updateMatomoInstance}
                    />
                </MatomoProvider>
            </ThemeProvider>
        </>
    )
}

export default MyApp;
