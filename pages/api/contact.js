import nodemailer from 'nodemailer';
import { ContactModalId } from 'components/ContactModal';
import { CONTACT_MAIL } from 'config';

const transporter = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: 465,
  secure: true,
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASS,
  },
});

export default async (req, res) => {
  const { name, company, email, phone, message, type } = req.body;

  const subject =
    type === ContactModalId.contactSales
      ? `dajeh.de: Anfrage für Sales-Team von ${name} (${email})`
      : `dajeh.de: Anfrage für Testaccount von ${name} (${email})`;

  let text = `Name:  ${name} \n \nE-Mail: ${email} \n \nTelefon: ${phone} \n \nFirma: ${company} \n \n`;

  if (type === ContactModalId.contactSales) {
    text += `Nachricht: \n${message} `;
  }

  const mailerRes = await mailer({
    fromEmail: email,
    name,
    subject,
    text,
  });

  res.send(mailerRes);
};

const mailer = async ({ fromEmail, subject, text }) => {
  const from = process.env.MAIL_USER;
  const message = {
    from,
    to: CONTACT_MAIL,
    subject: subject,
    text,
    replyTo: fromEmail,
  };

  return new Promise((resolve, reject) => {
    transporter.sendMail(message, (error, info) =>
      error ? reject(error) : resolve(info)
    );
  });
};
