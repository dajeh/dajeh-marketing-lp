const Theme = {
    colors: {
        primary: 'rgb(90, 200, 250)',
    },
}

export const Breakpoints = {
    Tablet: '600px',
    Desktop: '900px'
}

export default Theme;
