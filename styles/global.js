import { createGlobalStyle } from 'styled-components';
import Theme, { Breakpoints } from './theme';

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  
  .btn {
    display: block;
    padding: 13px 32px 15px 32px;
    min-height: 48px;
    text-decoration: none !important;
    text-align: center;
    outline: none;
    letter-spacing: 0;
    cursor: pointer;
    touch-action: manipulation;
    user-select: none;
    border: 0;
    border-radius: 24px;
    font-weight: 600;
  }
  
  .btn--primary {
    background-color: ${Theme.colors.primary};
    color: white;
    box-shadow: 0 1px 3px 0 rgba(102, 55, 9, 0.25);
  }
  
  @media (min-width: ${Breakpoints.Tablet}) {
    .btn {
      display: inline-block;
      text-align: center;  
    }
  }

  @media (min-width: ${Breakpoints.Desktop}) {
    .btn {
      min-width: auto;
    }
  }

  &:hover,
  &:focus {
    background-color: $color-btn-hover;
    color: $color-btn-txt;
  }

  &:active {
    background-color: $color-btn-active;
    color: $color-btn-txt;
  }
  
`

export default GlobalStyle;
