#!/bin/bash

##
## Deploy to staging (zelos2)
##

# the directory of the script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Versioning
PACKAGE_VERSION=$(grep version "${DIR}/package.json" | awk -F \" '{print $4}')
BUILD=${1-'1'}

##
## Deployment
##
IMAGE_VERSION="${PACKAGE_VERSION}-${BUILD}"
IMAGE_NAME="dajeh-website:${IMAGE_VERSION}"
ARCHIVE_NAME="dajeh-website_${IMAGE_VERSION}.tar"

##
## Build
##
echo "Building '${IMAGE_NAME}'"
docker build . -t "${IMAGE_NAME}"

# Save image to archive
docker save -o "${DIR}/build/${ARCHIVE_NAME}" "${IMAGE_NAME}"

##
## Copy image to zelos2
##
rsync "${DIR}/build/${ARCHIVE_NAME}" zelos2:docker-images --progress

##
## Remote action
##

# Load image into zelos2 docker
ssh zelos2 "docker load -i ~/docker-images/${ARCHIVE_NAME}"

echo "Image deployed to zelos2 - Manual container update necessary. Please ssh into zelos2 and:"
echo "1) plesk login"
echo "2) Docker -> dajeh-website -> Neu erstellen"
echo "3) Choose Image-Version '${IMAGE_VERSION}' and apply"
