## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Docker

Build Image

```bash
docker build . -t dajeh-website
```

Run Image

```bash
docker run -p 3000:3000 dajeh-website:1.0.0
```

## Deploy to staging

When building & deploying version X (e.g. `1.0.1`)

1. Locally:

        $ docker build . -t dajeh-website:1.0.1
        $ docker save -o dajeh-website_1_0_1.tar dajeh-website:1.0.1
        $ rsync dajeh-website.tar zelos2:docker-images --progress

2. On Zelos-2 (Staging):

    1. Load image
    
            $ docker load -i ~/docker-images/dajeh-website_1_0_1.tar

    3. (Re-)start server in Plesk using the new image

## Deploy to production

$ rsync dajeh-website_1_0_1.tar athletia2:docker-images --progress

$ docker load -i ~/docker-images/dajeh-website_1_0_1.tar

(Re-)start server in Plesk using the new image
