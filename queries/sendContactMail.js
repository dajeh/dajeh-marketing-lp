import axios from 'axios';

const sendContactMail = async (payload) => {
  return await axios({
    method: 'post',
    url: '/api/contact',
    headers: {
      'Content-Type': 'application/json',
    },
    data: payload,
  });
};

export default sendContactMail;
