import axios from 'axios';

const sendNewsletterRegistration = async (payload) => {
  return await axios({
    method: 'post',
    url: '/api/newsletter',
    headers: {
      'Content-Type': 'application/json',
    },
    data: payload,
  });
};

export default sendNewsletterRegistration;
