import { SITE_TITLE } from './config';

export function getPageTitle(pageTitle) {
    if(!pageTitle) {
        return SITE_TITLE;
    }

    return `${pageTitle} | ${SITE_TITLE}`
}
