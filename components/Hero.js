import Image from 'next/image';
import Button from './common/Button';
import Section from './common/Section';
import styled from 'styled-components'

const Title = styled.h1`
  font-size: 30px;
  line-height: 1.2;
  font-weight: 800;
  margin-bottom: 16px;
  
  @media (min-width: 700px) {
    font-size: 36px;
    max-width: 11em;
    margin-bottom: 24px;
  }
  
  @media (min-width: 900px) {
    font-size: 48px;
    margin-bottom: 32px;
  }
`

const SubTitle = styled.p`
  margin-bottom: 24px;
  
  @media (min-width: 700px) {
    max-width: 20em;
    margin-bottom: 32px;
  }

  @media (min-width: 900px) {
    margin-bottom: 48px;
  }
`

const Hero = ({ onClickTryForFree }) => {
  return (
    <Section
      bgColor="bg-white"
      containerStyles="flex flex-col md:flex-row items-center justify-between"
    >
      <div className="order-2 mt-12 md:mt-0 md:order-1 w-full md:w-6/12 lg:w-7/12 xl:w-6/12 mb-6 ">
        <Title>Dajeh – Zeiterfassung leicht gemacht</Title>
        <SubTitle className="lead-copy">
          Wir haben Arbeits&shy;zeit&shy;erfassung neu gedacht – kinder&shy;leicht und ohne Kopf&shy;schmerzen.<br/>
        </SubTitle>

        <Button
          containerStyles="mb-2 sm:w-auto"
          label="Jetzt kostenlos testen"
          onClick={onClickTryForFree}
        />

        <div className="text-xs text-center md:text-left lg:text-sm text-gray-600">
          Keine Einrichtung notwendig. 14 Tage kostenlos und unverbindlich testen.
        </div>
      </div>
      <div className="order-1 md:order-2 w-full md:w-6/12 lg:w-5/12 xl:w-6/12 flex items-center justify-center lg:justify-end">
        <Image src="/header_visual.png" width={440} height={505} priority />
      </div>
    </Section>
  );
};

export default Hero;
