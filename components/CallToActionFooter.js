import Button from './common/Button';
import Section from './common/Section';

const CallToActionFooter = ({ openContactSalesModal }) => {
    return (
        <Section bgColor="bg-dajeh-gray-500" containerStyles="sm:text-center">
            <h2 className="mb-4">Einfach mehr Zeit</h2>
            <p className="copy mb-6  md:w-10/12 lg:w-7/12  mx-auto">
                Schaffen Sie sich und Ihren Angestellten mehr Zeit für's Wesentliche.<br/>
                Mit der smarten und intuitiven Arbeitszeiterfassung Dajeh.
            </p>
            <Button
                containerStyles="sm:w-auto"
                onClick={openContactSalesModal}
                label="Sales-Team kontaktieren"
            />
        </Section>
    );
};

export default CallToActionFooter;
