import { useMatomo } from '@datapunt/matomo-tracker-react';
import { useState } from 'react';
import Logo from './common/Logo';
import ContactModal, { ContactModalId } from './ContactModal';

const Navigation = () => {
    const [contactSalesModalIsOpen, setContactSalesModalIsOpen] = useState(false);
    const [requestTestAccountModalIsOpen, setRequestTestAccountModalIsOpen] =
        useState(false);

    const { trackEvent } = useMatomo()

    const onClickContactSales = () => {
        setContactSalesModalIsOpen(true)
        trackEvent({
            category: 'contact-sales',
            action: 'click',
            name: 'nav'
        })
    }

    const onClickTryForFree = () => {
        setRequestTestAccountModalIsOpen(true);
        trackEvent({
            category: 'try-for-free',
            action: 'click',
            name: 'nav'
        })
    }

    return (
        <>
            <div className="h-14">
                <div className="bg-white w-full border-b fixed top-0 z-40 h-14 border-gray-200">
                    <div
                        className="container mx-auto px-4 sm:px-8 justify-between items-center h-14 flex">
                        <Logo />
                        <div className="flex">
                            <button
                                onClick={onClickContactSales}
                                className="text-dajeh-blue-500 hidden sm:block font-bold sm:mr-6"
                            >
                                Sales-Team kontaktieren
                            </button>
                            <button
                                onClick={onClickTryForFree}
                                className="sm:bg-dajeh-blue-500  sm:hover:bg-dajeh-blue-600 text-dajeh-blue-500   sm:text-white   sm:text-base font-bold sm:py-2 sm:px-8 sm:rounded-full"
                            >
                                Kostenlos testen
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {contactSalesModalIsOpen && (
                <ContactModal
                    onClose={() => setContactSalesModalIsOpen(false)}
                    type={ContactModalId.contactSales}
                />
            )}

            {requestTestAccountModalIsOpen && (
                <ContactModal
                    onClose={() => setRequestTestAccountModalIsOpen(false)}
                    type={ContactModalId.requestTestAccount}
                />
            )}
        </>
    );
};

export default Navigation;
