import { Formik } from 'formik';
import React, { useState } from 'react';
import TextInput from './common/Form/TextInput';
import Button from './common/Button';
import TextAreaInput from './common/Form/TextAreaInput';
import Modal from './common/Modal';
import sendContactMail from '../queries/sendContactMail';
import {
  MESSAGE_SUCCESSFUL_CONTACTFORM_SUBMISSION,
  MESSAGE_FAILED_CONTACTFORM_SUBMISSION,
} from 'config';
import Link from 'next/link';
import Checkbox from './common/Form/Checkbox';

export const initialValues = {
  name: '',
  company: '',
  email: '',
  phone: '',
  message: '',
  consent: false,
};

export const ContactModalId = {
  contactSales: 'contactSales',
  requestTestAccount: 'requestTestAccount',
};

export const ContactModalType = {
  contactSales: {
    title: 'Sales-Team kontaktieren',
    desc: 'Bitte teilen Sie uns Ihre Kontaktinformationen mit. Wir werden uns umgehend bei Ihnen melden.',
    btnLabel: 'Sales-Team kontaktieren',
  },
  requestTestAccount: {
    title: 'Kostenloses Testkonto anfragen',
    desc: 'Bitte teilen Sie uns Ihre Kontaktinformationen mit. Wir werden uns anschließend bei Ihnen melden, um Ihr Konto zu aktivieren.',
    btnLabel: 'Dajeh-Team kontaktieren',
  },
};

export default function ContactModal({ type, onClose }) {
  const [successMessage, setSuccessMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const onSubmit = async (values) => {
    const { name, company, email, phone, message } = values;

    setSuccessMessage('');
    setErrorMessage('');

    const payload = {
      name,
      company,
      email,
      phone,
      message,
      type,
    };

    try {
      await sendContactMail(payload);
      setSuccessMessage(MESSAGE_SUCCESSFUL_CONTACTFORM_SUBMISSION);
    } catch (error) {
      setErrorMessage(MESSAGE_FAILED_CONTACTFORM_SUBMISSION);
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      validate={(values) => {
        const errors = {};
        if (!values.email) {
          errors.email = 'Bitte geben Sie Ihre E-Mail Adresse ein.';
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = 'Bitte geben Sie eine gültige E-Mail Adresse ein.';
        }

        if (!values.name) {
          errors.name = 'Bitte geben Sie Ihren Namen ein';
        }

        if (!values.consent) {
          errors.consent =
            'Bitte bestätigen Sie die Verarbeitung Ihrer Daten  zum Zwecke der Kontaktaufnahme ';
        }

        return errors;
      }}
      onSubmit={async (values, { setSubmitting }) => {
        await onSubmit(values);
        setSubmitting(false);
      }}
    >
      {({
        values,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <form onSubmit={handleSubmit}>
          <Modal onClose={onClose}>
            <div className="text-center">
              <h3 className="text-2xl font-black mb-3">
                {ContactModalType[type].title}
              </h3>
              <p className="text-gray-500">{ContactModalType[type].desc}</p>
            </div>
            <div>
              {successMessage && (
                <div className="px-4 py-3 border border-green-400 flex rounded-2xl items-start relative mb-4 bg-green-100 ">
                  <span
                    className="relative text-green-600   mr-3 "
                    style={{ top: '2px' }}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      class="h-6 w-6"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                      />
                    </svg>
                  </span>
                  <span class="text-green-600 mr-8">{successMessage}</span>
                </div>
              )}
              {errorMessage && (
                <div className="px-4 py-3 border border-red-400 flex rounded-2xl items-start relative mb-4 bg-red-100 ">
                  <span
                    className="relative text-red-600   mr-3 "
                    style={{ top: '2px' }}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      class="h-6 w-6"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                      />
                    </svg>
                  </span>
                  <span class="text-red-600 mr-8">{errorMessage}</span>
                </div>
              )}
              <TextInput name="name" placeholder="Name*" />
              <TextInput
                name="company"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.company}
                placeholder="Firma"
                errors={errors}
              />
              <TextInput
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
                placeholder="E-Mail Adresse*"
                errors={errors}
              />
              <TextInput
                name="phone"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.phone}
                placeholder="Telefonnummer"
                errors={errors}
              />
              {type === ContactModalId.contactSales && (
                <TextAreaInput
                  name="message"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.message}
                  placeholder="Anfrage"
                  errors={errors}
                />
              )}

              <div className="text-xs mb-4 text-gray-600 ">
                Wir nehmen Ihre Datensicherheit ernst. Ihre Daten werden im
                Rahmen der DSGVO verwaltet und niemals weitergegeben.{' '}
                <Link href="/datenschutz">
                  <a className="text-dajeh-blue-600">
                    Weitere Informationen zum Datenschutz.
                  </a>
                </Link>
              </div>

              <Checkbox
                name="consent"
                label="Ich stimme der Verarbeitung meiner Daten zum Zwecke der Kontaktaufnahme zu*"
              />

              <Button
                containerStyles="w-full"
                label={
                  isSubmitting
                    ? 'Sende Anfrage...'
                    : ContactModalType[type].btnLabel
                }
                onClick={handleSubmit}
                disabled={isSubmitting}
              />
            </div>
          </Modal>
        </form>
      )}
    </Formik>
  );
}
