import Head from 'next/head';
import { useEffect } from 'react';
import { getPageTitle } from '../functions';
import Footer from './Footer';
import Navigation from './Header';
import { useMatomo } from '@datapunt/matomo-tracker-react'

const Layout = ({ title, children }) => {

    const { trackPageView } = useMatomo()

    // Track page view
    useEffect(() => {
        trackPageView({})
    }, [])

    return (
        <div>
            <Head>
                <title>{getPageTitle(title)}</title>
                <link rel="shortcut icon" href="/favicon.ico" />
                <link rel="preconnect" href="https://fonts.gstatic.com" />
                <link
                    href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;700;900&display=swap"
                    rel="stylesheet"
                />
            </Head>
            <Navigation />
            <div>{children}</div>
            <Footer />
        </div>
    );
}

export default Layout;
