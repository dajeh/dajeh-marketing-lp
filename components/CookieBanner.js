import CookieConsent from 'react-cookie-consent';
import styled from 'styled-components';

const CookieConsentCt = styled.div`
  .cc-ct {
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    background: #FFFFFF;
    box-shadow: 0 -3px 3px 0 rgba(0, 0, 0, 0.15);
    font-size: 14px;
    line-height: 20px;
    padding: 16px;

    @media (min-width: 700px) {
      bottom: 16px;
      right: 16px;
      left: auto;
      max-width: 350px;
      padding: 20px 24px 20px;
      box-shadow: 0 3px 3px 0 rgba(0, 0, 0, 0.12);
      border-radius: 8px;
    }
  }

  .cc-cnt a {
    text-decoration: underline;
  }

  .cc-link {
    opacity: 1;
    padding: 0;
    display: inline;
  }

  .cc-btn-wrp {
    text-align: right;
    margin-top: 16px;
  }

  .cc-btn {
    display: inline-block;
    text-align: center;

    &.btn {
      min-height: 0;
      padding: 6px 16px;
    }

    &.btn--primary {
      width: 40%;
    }
  }

  .cc-btn--decline {
    float: left;
    margin-left: -16px;
    font-weight: normal;
  }
`

export const CookieBanner = ({ onAccept, onDecline }) => (
    <CookieConsentCt>
        <CookieConsent
            disableStyles
            enableDeclineButton
            containerClasses="cc-ct"
            contentClasses="cc-cnt"
            buttonWrapperClasses="cc-btn-wrp"
            buttonClasses="btn btn--primary cc-btn"
            declineButtonClasses="btn btn--link cc-btn cc-btn--decline"
            overlayClasses="cc-overlay"
            onDecline={onDecline}
            onAccept={onAccept}
            buttonText="Okay"
            declineButtonText="Deaktivieren"
            location="none"
        >
            Um unsere Dienste ständig zu verbessern, nutzen wir den datenschutz&shy;freundlichen
            Tracking-Cookie von Matomo. Die anonym erhobenen Daten werden nicht mit Dritten geteilt.
            Weitere Informationen gibt's hier: <a
            href="/datenschutz">Datenschutz</a>
        </CookieConsent>
    </CookieConsentCt>
)
