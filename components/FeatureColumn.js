import Image from 'next/image';

const Feature = ({ image, title, desc }) => {
  //   const { width, height, path, alt } = image || undefined;
  return (
    <>
      <div className="mb-4 flex justify-between lg:flex-col">
        <div className="mr-6  flex items-start justify-center py-2 w-16 lg:items-center lg:mb-4 lg:h-28 lg:w-full lg:mr-0">
          <Image
            src={image?.path}
            alt={image?.alt}
            width={image?.width}
            priority
            height={image?.height}
          />
        </div>

        <div className="w-full lg:text-center">
          <h3 className="mb-2">{title}</h3>
          <p className="copy">{desc}</p>
        </div>
      </div>
    </>
  );
};

export default Feature;
