import styles from 'styles/Section.module.css';

const Section = ({
  children,
  wrapperStyles,
  noBottomPadding,
  bgColor,
  containerStyles,
}) => {
  return (
    <div className={`${bgColor} ${wrapperStyles}`}>
      <div
        className={`${styles.section} ${containerStyles} ${
          noBottomPadding ? 'pt-8 md:pt-12 lg:pt-16' : 'py-8 md:py-12 lg:py-16'
        }`}
      >
        {children}
      </div>
    </div>
  );
};

export default Section;
