import React from 'react';
import Image from 'next/image';

export const Modal = ({ children, onClose }) => {
  return (
    <div className="modal opacity-1 z-50 fixed w-full h-full top-0 left-0 flex items-center justify-center">
      <div
        className="modal-overlay absolute w-full h-full  bg-black opacity-25 top-0 left-0 cursor-pointer"
        onClick={onClose}
      ></div>
      <div className="absolute overflow-auto w-full px-4 top-0 bottom-0 sm:rounded-2xl sm:bottom-auto sm:top-auto sm:max-w-lg sm:px-14 py-20 z-50 bg-white   shadow-lg">
        <button className="absolute right-7 top-7" onClick={onClose}>
          <Image src="/close.svg" width={24} height={24} />
        </button>
        {children}
      </div>
    </div>
  );
};

export default Modal;
