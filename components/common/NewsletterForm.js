import {
  MESSAGE_SUCCESSFUL_NL_SUBMISSION,
  MESSAGE_FAILED_NL_SUBMISSION,
} from 'config';
import { Formik, Field } from 'formik';
import Image from 'next/image';
import sendNewsletterRegistration from 'queries/newsletterRegistration';
import { useState } from 'react';
const NewsletterForm = () => {
  const [successMessage, setSuccessMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const onSubmit = async (values) => {
    const { email } = values;

    setSuccessMessage('');
    setErrorMessage('');

    const payload = {
      email,
    };

    try {
      await sendNewsletterRegistration(payload);
      setSuccessMessage(MESSAGE_SUCCESSFUL_NL_SUBMISSION);
    } catch (error) {
      setErrorMessage(MESSAGE_FAILED_NL_SUBMISSION);
    }
  };
  return (
    <Formik
      initialValues={{ email: '' }}
      validate={(values) => {
        const errors = {};
        if (!values.email) {
          errors.email = 'Bitte geben Sie Ihre E-Mail Adresse ein.';
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = 'Bitte geben Sie eine gültige E-Mail Adresse ein.';
        }

        return errors;
      }}
      onSubmit={async (values, { setSubmitting }) => {
        await onSubmit(values);
        setSubmitting(false);
      }}
    >
      {({ handleSubmit, isSubmitting }) => (
        <form onSubmit={handleSubmit}>
          <div className="flex relative w-full ">
            <Field name="email">
              {({ field, meta }) => (
                <>
                  <input
                    className="rounded-full w-full z-0 py-3 px-5 border-t mr-0 border-b border-l text-gray-900 border-gray-200 bg-gray-100"
                    type="text"
                    placeholder="E-Mail Adresse eingeben"
                    {...field}
                  />
                  {meta.touched && meta.error && (
                    <div className="text-red-400 absolute -bottom-6 text-sm ">
                      {meta.error}
                    </div>
                  )}
                </>
              )}
            </Field>
            <button
              className="rounded-tr-full absolute z-1 right-0 bottom-0 top-0 w-12 rounded-br-full bg-dajeh-blue-500 hover:bg-dajeh-blue-600  text-white font-bold border-t py-1 px-4 border-b border-r"
              onClick={handleSubmit}
              disabled={isSubmitting}
              type="button"
            >
              <Image src="/chevron-right.svg" width={20} height={20} />
            </button>
          </div>
          {successMessage && (
            <div className="mt-4  text-green-600 ">{successMessage}</div>
          )}
          {errorMessage && (
            <div className="mt-4  text-red-400 ">{errorMessage}</div>
          )}
        </form>
      )}
    </Formik>
  );
};

export default NewsletterForm;
