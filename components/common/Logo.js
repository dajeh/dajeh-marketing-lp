import Image from 'next/image';
import Link from 'next/link';

const Logo = () => {
  return (
    <Link href="/">
      <div className="cursor-pointer flex items-center">
        <Image
          src="/dajeh_logo.png"
          alt="Dajeh - Zeiterfassung neu gedacht"
          width={87}
          height={24}
          unoptimized
        />
      </div>
    </Link>
  );
};

export default Logo;
