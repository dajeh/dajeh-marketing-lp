import { useMatomo } from '@datapunt/matomo-tracker-react';
import { APPSTORE_LINK, PLAYSTORE_LINK } from 'config';
import Image from 'next/image';

const AppStoreButtons = ({ matomoName }) => {
    const { trackEvent } = useMatomo()

    const onClickDownloadInAppStore = () => {
        onClickDownload('app-store')
    }

    const onClickDownloadInPlayStore = () => {
        onClickDownload('play-store')
    }

    const onClickDownload = (store) => {
        trackEvent({
            category: `download-app-${store}`,
            action: 'click',
            name: matomoName
        })
    }

    return (
        <div className="flex">
            <div className="cursor-pointer mr-6 w-52">
                <a target="_blank" href={APPSTORE_LINK} onClick={onClickDownloadInAppStore}>
                    <Image src="/app_store_badge.png" width={600} height={338} />
                </a>
            </div>

            <div className="cursor-pointer w-52">
                <a target="_blank" href={PLAYSTORE_LINK} onClick={onClickDownloadInPlayStore}>
                    <Image src="/play_store_badge.png" width={600} height={338} />
                </a>
            </div>
        </div>
    );
};

export default AppStoreButtons;
