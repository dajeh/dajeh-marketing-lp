const Row = ({ children }) => {
  return (
    <>
      <div className="flex flex-col md:flex-row md:flex-wrap -ml-4 -mr-4">
        {children}
      </div>
    </>
  );
};

export default Row;
