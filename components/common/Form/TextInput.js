import { Field } from 'formik';

const TextInput = ({ placeholder, styles, name }) => {
  return (
    <>
      <Field name={name}>
        {({ field, form: { touched, errors }, meta }) => (
          <div className="mb-4">
            <input
              className="rounded-full w-full z-0  py-3 px-6 border   text-gray-900 border-gray-200 bg-white"
              type="text"
              placeholder={placeholder}
              {...field}
            />
            {meta.touched && meta.error && (
              <div className="text-red-400 text-sm py-1 ml-6">{meta.error}</div>
            )}
          </div>
        )}
      </Field>
    </>
  );
};

export default TextInput;
