import { Field } from 'formik';

const TextAreaInput = ({ placeholder, name }) => {
  return (
    <>
      <Field name={name}>
        {({ field, form: { touched, errors }, meta }) => (
          <div className="mb-4">
            <textarea
              className="rounded-3xl w-full z-0  py-3 px-6 border   text-gray-900 border-gray-200 bg-white"
              type="text"
              placeholder={placeholder}
              rows="4"
              {...field}
            ></textarea>
            {meta.touched && meta.error && (
              <div className="text-red-400 text-xs py-2 ml-6">{meta.error}</div>
            )}
          </div>
        )}
      </Field>
    </>
  );
};

export default TextAreaInput;
