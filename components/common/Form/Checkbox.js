import { Field } from 'formik';

const Checkbox = ({ placeholder, styles, name, label }) => {
  return (
    <>
      <Field name={name}>
        {({ field, form: { touched, errors }, meta }) => (
          <div className="mb-4">
            <label className="flex cursor-pointer">
              <input
                type="checkbox"
                className="form-checkbox relative top-1 rounded h-4 mr-2 w-4 text-dajeh-blue-500"
                checked={field.value}
                {...field}
              />
              <div className=" text-sm">{label}</div>
            </label>
            {meta.touched && meta.error && (
              <div className="text-red-400 text-sm py-1 ml-6">{meta.error}</div>
            )}
          </div>
        )}
      </Field>
    </>
  );
};

export default Checkbox;
