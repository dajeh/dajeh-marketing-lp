const Button = ({ label, containerStyles, disabled, onClick, ...btnProps }) => {
  return (
    <button
      onClick={onClick}
      disabled={disabled}
      className={`${containerStyles} disabled:opacity-40 bg-dajeh-blue-500 hover:bg-dajeh-blue-600  text-white w-full text-base font-bold py-3 px-10 sm:px-14 rounded-full`}
    >
      {label}
    </button>
  );
};

export default Button;
