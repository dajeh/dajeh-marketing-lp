const Column = ({ children }) => {
  return (
    <>
      <div className="w-full mb-4 md:w-6/12 lg:w-4/12 lg:mb-4 px-4">
        {children}
      </div>
    </>
  );
};

export default Column;
