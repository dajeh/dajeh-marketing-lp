import Image from 'next/image';
import AppStoreButtons from './common/AppStoreButtons';
import Section from './common/Section';

const AppStore = () => {
    return (
        <Section
            containerStyles="flex flex-col items-center sm:flex-row sm:flex-row-reverse sm:justify-between xl:justify-center">
            <div className="w-full mb-8 sm:w-4/6 lg:w-7/12 xl:w-6/12 xl:max-w-md ">
                <h2 className="mb-4">Dajeh App</h2>
                <p className="mb-8 copy">
                    Im Web, für iOS und Android. Laden Sie sich die App herunter und starten Sie durch.
                </p>
                <AppStoreButtons matomoName="home" />
            </div>
            <div
                className="w-48 lg:w-4/12  xl:w-6/12 sm:mr-12 xl:mr-0 flex justify-center items-center ">
                <Image src="/app_store_visual.png" width={343} height={707} priority />
            </div>
        </Section>
    );
};

export default AppStore;
