import Column from './common/Column';
import Row from './common/Row';
import Section from './common/Section';
import Feature from './FeatureColumn';

const Features = () => {
    return (
        <Section bgColor="bg-dajeh-gray-500">
            <Row>
                <Column>
                    <Feature
                        image={{
                            path: '/bulp.svg',
                            alt: '',
                            width: 64,
                            height: 85,
                        }}
                        title="Einfach & intuitiv"
                        desc="Planen, erfassen und auswerten. Per App und Web-App."
                    />
                </Column>
                <Column>
                    <Feature
                        image={{
                            path: '/docs.svg',
                            alt: '',
                            width: 67,
                            height: 85,
                        }}
                        title="Smarte Berichte"
                        desc="Übersichtlich, intelligent und exportoptimiert."
                    />
                </Column>
                <Column>
                    <Feature
                        image={{
                            path: '/clock.svg',
                            alt: '',
                            width: 63,
                            height: 73,
                        }}
                        title="Online & Offline"
                        desc="Arbeitszeit immer und überall auch Offline erfassen."
                    />
                </Column>

                <Column>
                    <Feature
                        image={{
                            path: '/cloud.svg',
                            alt: '',
                            width: 82,
                            height: 70,
                        }}
                        title="Cloudbasiert"
                        desc="Einsatz von State-of-the-Art Cloud-Technologien. Permanente Backups."
                    />
                </Column>

                <Column>
                    <Feature
                        image={{
                            path: '/safety.svg',
                            alt: '',
                            width: 81,
                            height: 81,
                        }}
                        title="Bester Datenschutz"
                        desc="DSGVO-konform, entwickelt und gehostet in Deutschland."
                    />
                </Column>


                <Column>
                    <Feature
                        image={{
                            path: '/hands.svg',
                            alt: '',
                            width: 102,
                            height: 66,
                        }}
                        title="Keine versteckten Kosten"
                        desc="Flexible Abonnements, transparent und monatlich kündbar."
                    />
                </Column>
            </Row>
        </Section>
    );
};

export default Features;
