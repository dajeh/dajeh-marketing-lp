import Section from './common/Section';
import NewsletterForm from 'components/common/NewsletterForm';
import Link from 'next/link';
import Logo from './common/Logo';
import AppStoreButtons from './common/AppStoreButtons';
const Footer = () => {
  return (
    <Section
      wrapperStyles={'border-t border-gray-300'}
      containerStyles={'pb-0'}
      bgColor="bg-white"
      noBottomPadding
    >
      <div className="flex flex-col-reverse sm:flex-row sm:justify-between">
        <div className="mb-8 sm:w-6/12 xl:w-96 sm:pr-6">
          <div className="mb-2">
            <Logo />
          </div>
          <div className="mb-6 font-bold text-xl text-gray-500">
            So geht Zeiterfassung
          </div>
          <AppStoreButtons matomoName="footer" />
        </div>
        <div className="newsletter mb-12 sm:mb-8 sm:w-6/12 xl:w-96 sm:pl-6 md:pl-0">
          <h4 className="mb-3">Newsletter</h4>
          <p className="text-base text-gray-700 mb-4">
            Melden Sie sich bei unserem Newsletter, um exklusive Inhalte und
            Informationen zu erhalten.
          </p>
          <NewsletterForm />
        </div>
      </div>

      <div className="text-sm border-t py-5 border-gray-200 text-gray-400 flex flex-col sm:flex-row justify-between">
        <div className="mb-2 sm:mb-0">
          Made with passion by Kalaflax © 2021
        </div>
        <div className="">
          <Link href="/datenschutz">
            <a className="mr-4 text-sm">Datenschutz</a>
          </Link>
          <Link href="/impressum">
            <a className="text-sm">Impressum</a>
          </Link>
        </div>
      </div>
    </Section>
  );
};

export default Footer;
