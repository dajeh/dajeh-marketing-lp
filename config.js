export const APPSTORE_LINK = 'https://apps.apple.com/ch/app/dajeh/id1556853951';
export const PLAYSTORE_LINK =
    'https://play.google.com/store/apps/details?id=de.kalaflax.dajeh';

export const CONTACT_MAIL = 'hallo@dajeh.de';

export const MESSAGE_SUCCESSFUL_CONTACTFORM_SUBMISSION =
    'Vielen Dank! Wir werden Ihre Anfrage umgehend bearbeiten.';
export const MESSAGE_FAILED_CONTACTFORM_SUBMISSION =
    'Ihre Anfrage konnte nicht gesendet werden. Bitte probieren Sie es in wenigen Minuten erneut.';
export const MESSAGE_SUCCESSFUL_NL_SUBMISSION =
    'Vielen Dank! Sie erhalten in Kürze eine E-Mail zur Bestätigung Ihrer Newsletter-Anmeldung.';
export const MESSAGE_FAILED_NL_SUBMISSION =
    'Ihre Anfrage konnte nicht gesendet werden. Bitte probieren Sie es in wenigen Minuten erneut.';


export const SITE_TITLE = 'Dajeh - Arbeitszeiterfassung neu gedacht'
